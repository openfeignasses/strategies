if [ ! -d "../database-migrator" ]; then
    echo "Error: Directory ../database-migrator doesn't exist."
    exit 1
fi
if [ ! -d "../commons-python" ]; then
    echo "Error: Directory ../commons-python doesn't exist."
    exit 1
fi
echo "Recompiling code ..."
cd ../commons-python
zip -r cryptobot-commons-latest.zip . -x venv/**\* -x .idea/**\* -x .git/**\*
cd ../strategies
docker build --build-arg SSH_PRIVATE_KEY="$(cat ../deploy_key)" \
             --build-arg COMMONS_PYTHON_SHASUM="$(sha1sum ../commons-python/cryptobot-commons-latest.zip  | awk '{print $1}')" \
             -t strategies .
echo "Executing ..."
docker run \
    --rm \
    --name strategies-calibration \
    -v $(pwd)/../commons-python:/commons-python \
    -e MYSQL_DATABASE=cryptobot \
    -e MYSQL_USER=avnadmin \
    -e MYSQL_PASSWORD=xl5dyoj29yoier6r \
    -e DATABASE_HOST=mysql-cryptobot-nicoric-8357.aivencloud.com \
    -e DATABASE_PORT=24836 \
    -e START_AS_CALIBRATOR=true \
    -e STRATEGY_TO_CALIBRATE=MACD \
    -e HYPERPARAMETERS_MACD='{"short_period": 34, "long_period": 94, "macd_ema_period": 25}' \
    -e HYPERPARAMETERS_RSI='{"period": 14, "short_threshold": 30, "long_threshold": 70}' \
    strategies \
    bash -c "pip install /commons-python/cryptobot-commons-latest.zip && cd src/main && python calibrate.py"
rm ../commons-python/cryptobot-commons-latest.zip