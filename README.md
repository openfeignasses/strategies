# strategies

This Python module is in charge of deciding if we should invest in a currency or not.

## Requirements

- git
- docker

## Local development

*Commands must be run from the root of this repo.*  
*The database should be up and running (see database-migrator module).*  
*Folders database-migrator and commons-python must live next by the current repo:*  
```
.
├── database-migrator
├── commons-python
├── strategies
├── ...
```

### Execute the module

TODO

### Run the tests

`./localci/test-behaviour.sh`

Unit tests: from IDE.

Offline:  
It is possible to run tests offline, but you need to run the command while being online at least once.

```
Delivered with 💓 by
                                                                                            
              /')                                                                           
            /' /' ____     O  ____     ,____     ____     ____     ____     ____     ____   
         -/'--' /'    )  /' /'    )   /'    )  /'    )  /'    )--/'    )--/'    )  /'    )--
        /'    /(___,/' /' /'    /'  /'    /' /'    /'  '---,    '---,   /(___,/'  '---,     
      /(_____(________(__(___,/(__/'    /(__(___,/(__(___,/   (___,/   (________(___,/      
    /'                      /'                                                              
  /'                /     /'                                                                
/'                 (___,/'                                                                  
```