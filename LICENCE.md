Cryptobot is a set of modules working together to automate a crypto-currencies investment fund.

All files in this repository are part of Cryptobot, except files in "src/main/strategies_private" and its sub-folders. This means that you can create your own strategies in "src/main/strategies_private" without sharing them to the community.

Copyright (C) 2021  Antoine ROUGEOT, Harold GUENEAU, Simon GIBARD, Julien RASPAUD, Tom CHAUVEAU

Cryptobot is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License.

Cryptobot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.