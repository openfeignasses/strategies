#!/bin/bash

set -e

# Start in watching mode for production
if [ -n "$KEEP_WATCHING_STOCK_QUOTATIONS" ]; then
    echo "Starting with keep watching stock_quotations table mode ..."
    while [ true ]
    do
        python src/main/should_run.py && python src/main/execute.py
        sleep 180 # loop every 3 minutes
    done
fi

if [ -n "$START_AS_CALIBRATOR" ]; then
  # Start with calibration mode
  exec "$@ && cat /workspace/calibrated_hyperparameters.json"
  # todo: save file content as environment variable
else
  # Start with what is defined in CMD
  exec "$@"
fi