import os
import traceback
from unittest import mock
from radish import given, before, when, then
from radish import custom_type
from strategies.inputs import StrategyEmptyInput
from strategies.inputs import StrategyInput
from strategies import SafetyFirst
from strategies import SmaCrossovers
from strategies import EmaCrossovers
from strategies import MACD
from strategies import RSI
from cryptobot_commons.repositories import StrategiesResultsRepository
from cryptobot_commons.retrievers import InvestmentUniverseStatusRetriever
from cryptobot_commons.retrievers import StrategiesResultsRetriever
from cryptobot_commons.savers import InvestmentUniverseStatusSaver
from cryptobot_commons.savers import StrategiesResultsSaver
from cryptobot_commons.hyperparameters import HyperparametersLoader
from cryptobot_commons import CryptoCurrencyPairBuilder
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons import InvestmentUniverseStatus
from cryptobot_commons import Signal
from cryptobot_commons import StrategyResult
from cryptobot_commons import StrategiesResults
from cryptobot_commons.utils import TextChartConverter
from utils import Utils


def convert_time_to_int(table_to_format:list):
    for line in table_to_format:
        line["time"] = int(line["time"])
    return table_to_format


def fill_open_high_low_vwap_volume_count(stock_quotations_for_currency_raw):
    for line in stock_quotations_for_currency_raw:
        line["open"] = 0
        line["high"] = 0
        line["low"] = 0
        line["vwap"] = 0
        line["volume"] = 0
        line["count"] = 0
    return stock_quotations_for_currency_raw


@custom_type("Any", r".*\n?")
def match_any(value):
    return value


@before.each_scenario
def clean_tables(scenario):
    scenario.context.strategies_results_repository = StrategiesResultsRepository()
    scenario.context.strategies_results_repository.truncate()


@before.each_scenario
def clean_investment_universe_status(scenario):
    investment_universe_status = InvestmentUniverseStatus([{
        "name": InvestmentUniverse.BTC_EUR,
        "status": "FORCE_IGNORE",
    }, {
        "name": InvestmentUniverse.LTC_EUR,
        "status": "FORCE_IGNORE",
    }])
    InvestmentUniverseStatusSaver(investment_universe_status).save()


@before.each_scenario
def init_dicts(scenario):
    scenario.context.crypto_currency_pairs = {}
    scenario.context.hyperparameters = {}


@given('the "{pair:w}" stock quotation table')
def step_impl(step, pair):
    formatted_table = convert_time_to_int(step.table)
    formatted_table = fill_open_high_low_vwap_volume_count(formatted_table)
    step.context.crypto_currency_pairs[pair] = CryptoCurrencyPairBuilder(pair, formatted_table).build()


@given('the "{pair:w}" stock quotation chart')
def step_impl(step, pair):
    stock_quotations_for_currency_raw = TextChartConverter(step.text).to_dicts()
    stock_quotations_for_currency_raw = fill_open_high_low_vwap_volume_count(stock_quotations_for_currency_raw)
    step.context.crypto_currency_pairs[pair] = CryptoCurrencyPairBuilder(
        pair,
        stock_quotations_for_currency_raw
    ).build()


@given('that the "{pair:w}" is flagged as TRADABLE')
def my_step(step, pair):
    investment_universe_status = InvestmentUniverseStatus([]).set_status(InvestmentUniverse[pair], "TRADABLE")
    InvestmentUniverseStatusSaver(investment_universe_status).save()


@given('that the "{pair:w}" is flagged as NON_TRADABLE')
def my_step(step, pair):
    investment_universe_status = InvestmentUniverseStatus([]).set_status(InvestmentUniverse[pair], "NON_TRADABLE")
    InvestmentUniverseStatusSaver(investment_universe_status).save()

@given('that the "{pair:w}" is flagged as CRASHING')
def my_step(step, pair):
    investment_universe_status = InvestmentUniverseStatus([]).set_status(InvestmentUniverse[pair], "CRASHING")
    InvestmentUniverseStatusSaver(investment_universe_status).save()

@given('that available hyperparameters for "{strategy_name:w}" are "{hyperparameters_json_string:Any}"')
def step_impl(step, strategy_name, hyperparameters_json_string):
    env_variables_mock = mock.patch.dict(os.environ, {"HYPERPARAMETERS_"+strategy_name: hyperparameters_json_string})
    env_variables_mock.start()
    step.context.hyperparameters[strategy_name] = HyperparametersLoader("HYPERPARAMETERS_"+strategy_name).load()
    env_variables_mock.stop()


@given('that the current strategy is SMA_CROSSOVERS')
def step_impl(step):
    step.context.strategy_to_execute = SmaCrossovers(step.context.hyperparameters["SMA_CROSSOVERS"])

@given('that the current strategy is EMA_CROSSOVERS')
def step_impl(step):
    step.context.strategy_to_execute = EmaCrossovers(step.context.hyperparameters["EMA_CROSSOVERS"])

@given('that the current strategy is MACD')
def step_impl(step):
    step.context.strategy_to_execute = MACD(step.context.hyperparameters["MACD"])

@given('that the current strategy is RSI')
def step_impl(step):
    step.context.strategy_to_execute = RSI(step.context.hyperparameters["RSI"])

@when('we execute the strategy')
def step_impl(step):
    strategies_results_list = []

    # TRADABLE Currency Pairs
    tradable_currencies = InvestmentUniverseStatusRetriever("TRADABLE").execute()
    for currency_pair in tradable_currencies.status:
        strategy_input = StrategyInput(step.context.crypto_currency_pairs[currency_pair.name])
        strategies_results_list.append(step.context.strategy_to_execute.calculate_signal(strategy_input))
        step.context.strategy_executed = step.context.strategy_to_execute.name

    # NON_TRADABLE Currency Pairs
    non_tradable_currencies = InvestmentUniverseStatusRetriever("NON_TRADABLE").execute()
    safety_first_strategy = SafetyFirst()
    for currency_pair in list(non_tradable_currencies.status.keys()):
        strategy_empty_input = StrategyEmptyInput(currency_pair)
        strategies_results_list.append(safety_first_strategy.calculate_signal(strategy_empty_input))
        step.context.strategy_executed = "SAFETY_FIRST"

    # CRASHING Currency Pairs
    crashing_currencies = InvestmentUniverseStatusRetriever("CRASHING").execute()
    safety_first_strategy = SafetyFirst()
    for crashing_currency in list(crashing_currencies.status.keys()):
        strategy_empty_input = StrategyEmptyInput(crashing_currency)
        strategies_results_list.append(safety_first_strategy.calculate_signal(strategy_empty_input))
        step.context.strategy_executed = "SAFETY_FIRST"

    strategies_results = StrategiesResults(strategies_results_list)
    strategies_results_saver = StrategiesResultsSaver(strategies_results)
    strategies_results_saver.save()


@when('we try to execute the strategy')
def step_impl(step):
    try:
        strategies_results_list = []

        # TRADABLE Currency Pairs
        tradable_currencies = InvestmentUniverseStatusRetriever("TRADABLE").execute()
        for currency_pair in tradable_currencies.status:
            strategy_input = StrategyInput(step.context.crypto_currency_pairs[currency_pair.name])
            strategies_results_list.append(step.context.strategy_to_execute.calculate_signal(strategy_input))
            step.context.strategy_executed = step.context.strategy_to_execute.name

        # NON_TRADABLE Currency Pairs
        non_tradable_currencies = InvestmentUniverseStatusRetriever("NON_TRADABLE").execute()
        safety_first_strategy = SafetyFirst()
        for currency_pair in list(non_tradable_currencies.status.keys()):
            strategy_empty_input = StrategyEmptyInput(currency_pair)
            strategies_results_list.append(safety_first_strategy.calculate_signal(strategy_empty_input))
            step.context.strategy_executed = "SAFETY_FIRST"

        strategies_results = StrategiesResults(strategies_results_list)
        strategies_results_saver = StrategiesResultsSaver(strategies_results)
        strategies_results_saver.save()

    except Exception:
        step.context.exception_detected = True
        traceback.print_exc()


@then('the strategy executed is "{strategy:w}"')
def step_impl(step, strategy):
    assert step.context.strategy_executed == strategy


@then('the type of the output is StrategyResult for "{pair:w}"')
def step_impl(step, pair):
    strategy_result = Utils.find_strategy_by_currency(StrategiesResultsRetriever("LAST_EXECUTION").execute(), pair)
    assert type(strategy_result) == StrategyResult


@then('the signal is "{expected_signal:w}" for "{pair:w}"')
def step_impl(step, expected_signal, pair):
    strategy_result = Utils.find_strategy_by_currency(StrategiesResultsRetriever("LAST_EXECUTION").execute(), pair)
    assert strategy_result.signal == Signal[expected_signal]


@then('an exception is thrown')
def step_impl(step):
    assert step.context.exception_detected == True
