-- Always mark pairs used in behaviour tests as tradable by default
INSERT INTO `cryptobot`.`investment_universe_status` (`name`, `status`) VALUES ('BTC_EUR', 'FORCE_IGNORE');
INSERT INTO `cryptobot`.`investment_universe_status` (`name`, `status`) VALUES ('LTC_EUR', 'FORCE_IGNORE');