@Strategies
@Strategies_Commons
Feature: Strategies are using a common code base

   As Jean-Michel, the Strategist
   In order to be able to use all the strategies in the same way although their internal functioning is different
   I want all the strategies to use the same code base.
   This code base defines what are the possible inputs and outputs for all the strategies and the functional logic of a strategy.

   Background:
      Given the "BTC_EUR" stock quotation chart
      """
      +---------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      +---------------------------------------------+
      """
      And that the "BTC_EUR" is flagged as TRADABLE
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 3, "long_period": 9}"
      And that available hyperparameters for "EMA_CROSSOVERS" are "{"short_period": 3, "long_period": 9}"
      And that available hyperparameters for "MACD" are "{"short_period": 12, "long_period": 26, "macd_ema_period": 9}"
      And that available hyperparameters for "RSI" are "{"period": 14, "short_threshold": 30, "long_threshold": 70}"

   @Strategies_Commons_1
   Scenario Outline: Output is a StrategyResult
      Given that the current strategy is <Strategy>
      When we execute the strategy
      Then the type of the output is StrategyResult for "BTC_EUR"
      Examples:
      | Strategy                     |
      | SMA_CROSSOVERS               |
      | EMA_CROSSOVERS               |
      | MACD                         |
      | RSI                          |

   @Strategies_Commons_2
   Scenario Outline: The strategy executed is the one expected
      Given that the current strategy is <Strategy>
      When we execute the strategy
      Then the strategy executed is "<Strategy>"
      Examples:
      | Strategy                     |
      | SMA_CROSSOVERS               |
      | EMA_CROSSOVERS               |
      | MACD                         |
      | RSI                          |

   @Strategies_Commons_3
   Scenario Outline: Multiple tradable currencies
      Given the "LTC_EUR" stock quotation chart
      """
      +---------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      +---------------------------------------------+
      """
      And that the "LTC_EUR" is flagged as TRADABLE
      And that the current strategy is <Strategy>
      When we execute the strategy
      Then the type of the output is StrategyResult for "BTC_EUR"
      And the type of the output is StrategyResult for "LTC_EUR"
      Examples:
      | Strategy                     |
      | SMA_CROSSOVERS               |
      | EMA_CROSSOVERS               |
      | MACD                         |
      | RSI                          |


   @Strategies_Commons_4
   Scenario Outline: Less than 10 values in the input time series
      Given the "BTC_EUR" stock quotation table
      | time   | close     |
      | 1      | 4379.55   |
      | 2      | 4439.247  |
      | 3      | 4612.203  |
      | 4      | 4779.151  |
      | 5      | 4770.347  |
      | 6      | 4826.102  |
      | 7      | 5441.446  |
      And that the "BTC_EUR" is flagged as TRADABLE
      And that the current strategy is <Strategy>
      When we try to execute the strategy
      Then an exception is thrown
      Examples:
      | Strategy                     |
      | SMA_CROSSOVERS               |
      | EMA_CROSSOVERS               |
      | MACD                         |
      | RSI                          |

   @Strategies_Commons_6
   Scenario Outline: The cryptocurrency pair is flagged as non tradable
      Given that the "BTC_EUR" is flagged as NON_TRADABLE
      And that the current strategy is <Strategy>
      When we execute the strategy
      Then the signal is "NEUTRAL" for "BTC_EUR"
      Examples:
      | Strategy                     |
      | SMA_CROSSOVERS               |
      | EMA_CROSSOVERS               |
      | MACD                         |
      | RSI                          |

   @Strategies_Commons_7
   Scenario Outline: The cryptocurrency pair is flagged as non tradable 2
      Given that the "BTC_EUR" is flagged as NON_TRADABLE
      And that the current strategy is <Strategy>
      When we execute the strategy
      Then the strategy executed is "SAFETY_FIRST"
      Examples:
      | Strategy                     |
      | SMA_CROSSOVERS               |
      | EMA_CROSSOVERS               |
      | MACD                         |
      | RSI                          |

   @Strategies_Commons_8
   Scenario Outline: The cryptocurrency pair is flagged as crashing
      Given that the "BTC_EUR" is flagged as CRASHING
      And that the current strategy is <Strategy>
      When we execute the strategy
      Then the strategy executed is "SAFETY_FIRST"
      Examples:
      | Strategy                     |
      | SMA_CROSSOVERS               |
      | EMA_CROSSOVERS               |
      | MACD                         |
      | RSI                          |
