@Strategies
@Strategies_SMA_Crossovers
Feature: Compute a signal using SMA Crossovers

  As Jean-Michel the Strategist
  In order to compute signals
  I want to be able to use the SMA Crossovers algorithm.
  This strategy should be usable for all currencies.

   Background:
      Given that the "BTC_EUR" is flagged as TRADABLE

   @Strategies_SMA_Crossovers_1
   Scenario: We compute a signal sucessfully using SMA Crossovers
      Given the "BTC_EUR" stock quotation chart
      """
      +---------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      +---------------------------------------------+
      """
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 3, "long_period": 9}"
      And that the current strategy is SMA_CROSSOVERS
      When we execute the strategy
      Then the type of the output is StrategyResult for "BTC_EUR"

   @Strategies_SMA_Crossovers_2
   Scenario: Stock quotation with an upward trend
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |                                           XXX|
      |                                         XXX  |
      |                                       XXX    |
      |                                     XXX      |
      |                                  XXXX        |
      |                                XXX           |
      |                           XXXXX              |
      |                        XXX                   |
      |                   XXXXX                      |
      |              XXXXX                           |
      |          XXXX                                |
      |        XX                                    |
      |    XXXX                                      |
      |XXXX                                          |
      +----------------------------------------------+
      """
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 3, "long_period": 5}"
      And that the current strategy is SMA_CROSSOVERS
      When we execute the strategy
      Then the signal is "LONG" for "BTC_EUR"

   @Strategies_SMA_Crossovers_3
   Scenario: Stock quotation with a downward trend
     Given the "BTC_EUR" stock quotation chart
      """
      +--------------------------------------------------------+
      |XXX                                                     |
      |   XXX                                                  |
      |     XXX                                                |
      |        XX                                              |
      |          XXX                                           |
      |             XXXX                                       |
      |                 XX                                     |
      |                   XX                                   |
      |                    XXX                                 |
      |                       XX                               |
      |                         XXX                            |
      |                           XXXXX                        |
      |                                XXXXXX                  |
      |                                      XXXX              |
      |                                         XXX            |
      |                                           XX           |
      |                                            XXX         |
      |                                               XX       |
      |                                                 XXX    |
      |                                                   XXXX |
      |                                                      XX|
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      +--------------------------------------------------------+
      """
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 3, "long_period": 9}"
      And that the current strategy is SMA_CROSSOVERS
      When we execute the strategy
      Then the signal is "SHORT" for "BTC_EUR"
      
   @Strategies_SMA_Crossovers_4
   Scenario: Stock quotation with no change
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------------------------------+
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      +----------------------------------------------------------------------+
      """
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 3, "long_period": 5}"
      And that the current strategy is SMA_CROSSOVERS
      When we execute the strategy
      Then the signal is "NEUTRAL" for "BTC_EUR"
      
   @Strategies_SMA_Crossovers_5
   Scenario: Stock quotation with recent change (downward)
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                             X|
      +----------------------------------------------+
      """
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 3, "long_period": 9}"
      And that the current strategy is SMA_CROSSOVERS
      When we execute the strategy
      Then the signal is "NEUTRAL" for "BTC_EUR"

   @Strategies_SMA_Crossovers_6
   Scenario: Stock quotation with recent change (upward)
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |                                             X|
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |                                              |
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |
      +----------------------------------------------+
      """
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 3, "long_period": 9}"
      And that the current strategy is SMA_CROSSOVERS
      When we execute the strategy
      Then the signal is "NEUTRAL" for "BTC_EUR"

   @Strategies_SMA_Crossovers_7
   Scenario: Short and long periods are the same
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      +----------------------------------------------+
      """
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 9, "long_period": 9}"
      And that the current strategy is SMA_CROSSOVERS
      When we try to execute the strategy
      Then an exception is thrown

   @Strategies_SMA_Crossovers_8
   Scenario: Short period is greater than the long period
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |
      +----------------------------------------------+
      """
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 9, "long_period": 3}"
      And that the current strategy is SMA_CROSSOVERS
      When we try to execute the strategy
      Then an exception is thrown

   @Strategies_SMA_Crossovers_9
   Scenario: Not enough data
      Given the "BTC_EUR" stock quotation table
      | time       | close     |
      | 1          | 4379.55   |
      | 2          | 4439.247  |
      | 3          | 4612.203  |
      | 4          | 4779.151  |
      | 5          | 4770.347  |
      | 6          | 4826.102  |
      | 7          | 5441.446  |
      | 8          | 5638.585  |
      | 9          | 5823.791  |
      | 10         | 5704.199  |
      | 11         | 5760.141  |
      | 12         | 5604.586  |
      | 13         | 5582.402  |
      | 14         | 5700.193  |
      | 15         | 5987.912  |
      | 16         | 6014.75   |
      | 17         | 6014.75   |
      | 18         | 6014.75   |
      | 19         | 6014.75   |
      | 20         | 6014.75   |
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 9, "long_period": 20}"
      And that the current strategy is SMA_CROSSOVERS
      When we try to execute the strategy
      Then an exception is thrown

   @Strategies_SMA_Crossovers_10
   Scenario: Enough data (max_period+1)
      Given the "BTC_EUR" stock quotation table
      | time       | close     |
      | 1          | 4379.55   |
      | 2          | 4439.247  |
      | 3          | 4612.203  |
      | 4          | 4779.151  |
      | 5          | 4770.347  |
      | 6          | 4826.102  |
      | 7          | 5441.446  |
      | 8          | 5638.585  |
      | 9          | 5823.791  |
      | 10         | 5704.199  |
      | 11         | 5760.141  |
      | 12         | 5604.586  |
      | 13         | 5582.402  |
      | 14         | 5700.193  |
      | 15         | 5987.912  |
      | 16         | 6014.75   |
      | 17         | 6014.75   |
      | 18         | 6014.75   |
      | 19         | 6014.75   |
      | 20         | 6014.75   |
      | 21         | 6014.75   |
      And that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 9, "long_period": 20}"
      And that the current strategy is SMA_CROSSOVERS
      When we execute the strategy
      Then the type of the output is StrategyResult for "BTC_EUR"

