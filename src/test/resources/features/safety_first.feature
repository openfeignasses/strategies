@Strategies @Safety_First
Feature: Take a NEUTRAL position to stay safe

  As Jean-Michel the Strategist
  In order to compute safe signals
  I want to be able to use the Safety First algorithm to take a NEUTRAL position.
  This strategy should be usable for all currencies.

  Background:
    Given that available hyperparameters for "SMA_CROSSOVERS" are "{"short_period": 3, "long_period": 9}"
    And that the current strategy is SMA_CROSSOVERS

  @Safety_First_1
  Scenario: Stock quotation with an upward trend (non-tradable)
    Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      +----------------------------------------------+
      """
    And that the "BTC_EUR" is flagged as TRADABLE
    And the "LTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |                                           XXX|
      |                                         XXX  |
      |                                       XXX    |
      |                                     XXX      |
      |                                  XXXX        |
      |                                XXX           |
      |                           XXXXX              |
      |                        XXX                   |
      |                   XXXXX                      |
      |              XXXXX                           |
      |          XXXX                                |
      |        XX                                    |
      |    XXXX                                      |
      |XXXX                                          |
      +----------------------------------------------+
      """
    And that the "LTC_EUR" is flagged as NON_TRADABLE
    When we execute the strategy
    Then the signal is "NEUTRAL" for "LTC_EUR"

  @Safety_First_2
  Scenario: Stock quotation with an downward trend (NON_TRADABLE)
    Given the "BTC_EUR" stock quotation chart
      """
      +--------------------------------------------------------+
      |XXX                                                     |
      |   XXX                                                  |
      |     XXX                                                |
      |        XX                                              |
      |          XXX                                           |
      |             XXXX                                       |
      |                 XX                                     |
      |                   XX                                   |
      |                    XXX                                 |
      |                       XX                               |
      |                         XXX                            |
      |                           XXXXX                        |
      |                                XXXXXX                  |
      |                                      XXXX              |
      |                                         XXX            |
      |                                           XX           |
      |                                            XXX         |
      |                                               XX       |
      |                                                 XXX    |
      |                                                   XXXX |
      |                                                      XX|
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      +--------------------------------------------------------+
      """
    And that the "BTC_EUR" is flagged as NON_TRADABLE
    And the "LTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      +----------------------------------------------+
      """
    And that the "LTC_EUR" is flagged as TRADABLE
    When we execute the strategy
    Then the signal is "NEUTRAL" for "BTC_EUR"


 @Safety_First_3
  Scenario: Stock quotation with an downward trend (NON_TRADABLE) 2
    Given the "BTC_EUR" stock quotation chart
      """
      +--------------------------------------------------------+
      |XXX                                                     |
      |   XXX                                                  |
      |     XXX                                                |
      |        XX                                              |
      |          XXX                                           |
      |             XXXX                                       |
      |                 XX                                     |
      |                   XX                                   |
      |                    XXX                                 |
      |                       XX                               |
      |                         XXX                            |
      |                           XXXXX                        |
      |                                XXXXXX                  |
      |                                      XXXX              |
      |                                         XXX            |
      |                                           XX           |
      |                                            XXX         |
      |                                               XX       |
      |                                                 XXX    |
      |                                                   XXXX |
      |                                                      XX|
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      |                                                        |
      +--------------------------------------------------------+
      """
    And that the "BTC_EUR" is flagged as NON_TRADABLE
    When we execute the strategy
    Then the strategy executed is "SAFETY_FIRST"




