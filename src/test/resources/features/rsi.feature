@Strategies
@Strategies_RSI
Feature: Compute a signal using RSI

  As Jean-Michel the Strategist
  In order to compute signals
  I want to be able to use the RSI algorithm.
  This strategy should be usable for all currencies.

   Background:
      Given that the "BTC_EUR" is flagged as TRADABLE

   @Strategies_RSI_1
   Scenario: Stock quotation with no change
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------------------------------+
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      |                                                                      |
      +----------------------------------------------------------------------+
      """
      And that available hyperparameters for "RSI" are "{"period": 14, "short_threshold": 30, "long_threshold": 70}"
      And that the current strategy is RSI
      When we execute the strategy
      Then the signal is "NEUTRAL" for "BTC_EUR"

   @Strategies_RSI_2
   Scenario: Short and long thresholds are the same
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      +----------------------------------------------+
      """
      And that available hyperparameters for "RSI" are "{"period": 14, "short_threshold": 50, "long_threshold": 50}"
      And that the current strategy is RSI
      When we try to execute the strategy
      Then an exception is thrown

   @Strategies_RSI_3
   Scenario: Short threshold is greater than the long threshold
      Given the "BTC_EUR" stock quotation chart
      """
      +----------------------------------------------+
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |
      +----------------------------------------------+
      """
      And that available hyperparameters for "RSI" are "{"period": 14, "short_threshold": 70, "long_threshold": 30}"
      And that the current strategy is RSI
      When we try to execute the strategy
      Then an exception is thrown

   @Strategies_RSI_4
   Scenario: Not enough data
      Given the "BTC_EUR" stock quotation table
      | time       | close     |
      | 1          | 4379.55   |
      | 2          | 4439.247  |
      | 3          | 4612.203  |
      | 4          | 4779.151  |
      | 5          | 4770.347  |
      | 6          | 4826.102  |
      | 7          | 5441.446  |
      | 8          | 5638.585  |
      | 9          | 5823.791  |
      | 10         | 5704.199  |
      | 11         | 5760.141  |
      | 12         | 5604.586  |
      | 13         | 5582.402  |
      | 14         | 5700.193  |
      And that available hyperparameters for "RSI" are "{"period": 14, "short_threshold": 30, "long_threshold": 70}"
      And that the current strategy is RSI
      When we try to execute the strategy
      Then an exception is thrown

   @Strategies_RSI_5
   Scenario: Enough data (max_period+1)
      Given the "BTC_EUR" stock quotation table
      | time       | close     |
      | 1          | 4379.55   |
      | 2          | 4439.247  |
      | 3          | 4612.203  |
      | 4          | 4779.151  |
      | 5          | 4770.347  |
      | 6          | 4826.102  |
      | 7          | 5441.446  |
      | 8          | 5638.585  |
      | 9          | 5823.791  |
      | 10         | 5704.199  |
      | 11         | 5760.141  |
      | 12         | 5604.586  |
      | 13         | 5582.402  |
      | 14         | 5700.193  |
      | 15         | 5987.912  |
      And that available hyperparameters for "RSI" are "{"period": 14, "short_threshold": 30, "long_threshold": 70}"
      And that the current strategy is RSI
      When we execute the strategy
      Then the type of the output is StrategyResult for "BTC_EUR"


