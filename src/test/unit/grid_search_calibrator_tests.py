import unittest
import pandas as pd

from strategies.inputs.strategy_input import StrategyInput
from strategies.strategy import Strategy
from calibrators import GridSearchCalibrator

from cryptobot_commons.strategy_result import StrategyResult
from cryptobot_commons.signal import Signal
from cryptobot_commons.crypto_currency_pair_builder import CryptoCurrencyPair
from cryptobot_commons import CryptoCurrencyPairs
from cryptobot_commons.hyperparameters import HyperparametersConstraints, HyperparametersGridGenerator
from cryptobot_commons.hyperparameters import Hyperparameters
from cryptobot_commons.hyperparameters import StringBoundaryConstraint


class Foo(Strategy):

    def __init__(self, hyperparameters: Hyperparameters):
        super().__init__("Foo")
        self.hyperparameters_dict = hyperparameters.entries
        self.hyperparameters_boundary_constraints = HyperparametersConstraints({
            "signal": StringBoundaryConstraint(['LONG', 'SHORT', 'NEUTRAL'])
        })
        self.hyperparameters_functional_constraints = None

    def generate_signal(self, strategy_input: StrategyInput):
        signal = Signal[self.hyperparameters_dict["signal"]]
        return signal
    
    def train(self):
        """Not necessary for this strategy"""
        pass


def generate_cryptocurrency_pair(quotes_list: list, name="BTC_EUR"):
    """initial timestamp = 1588496400
    However, we consider here 1588496400 - 10*3600 in order to have at least 10 data initially
    (necessary to execute the strategy)
    """
    timestamp_list = [1588496400 - 10 * 3600 + i * 3600 for i in range(len(quotes_list))]
    columns_names = ["open", "high", "low", "close", "vwap", "volume", "count"]
    df = pd.DataFrame(index=timestamp_list)
    for col in columns_names:
        df[col] = quotes_list
    return CryptoCurrencyPair(name=name, data=df)


class GridSearchCalibratorTestCase(unittest.TestCase):
    """
    We set nb_data_training=10 in order to have at least 10 data in the crypto currency pair.
    This is necessary to execute every strategy.
    """

    def setUp(self):
        self.foo_strategy = Foo(Hyperparameters({"signal": 'LONG'}))

    def test_scenario_1(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'LONG'})
        assert best_hyperparameters == expected_hyperparameters
    
    def test_scenario_2(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'SHORT'})
        assert best_hyperparameters == expected_hyperparameters
    
    def test_scenario_3(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9,
                       8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'NEUTRAL'})
        assert best_hyperparameters == expected_hyperparameters
    
    def test_scenario_1_consider_neutral_False(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10, 
                                                consider_neutral=False)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'LONG'})
        assert best_hyperparameters == expected_hyperparameters
    
    def test_scenario_2_consider_neutral_False(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10, 
                                                consider_neutral=False)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'SHORT'})
        assert best_hyperparameters == expected_hyperparameters
    
    def test_scenario_3_consider_neutral_False(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9,
                       8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10, 
                                                consider_neutral=False)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'SHORT'})
        assert best_hyperparameters == expected_hyperparameters
    
    def test_score_values_1(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.2222222222222222, 5)
        assert round(scores[1], 5) == round(0.06666666666666667, 5)
        assert round(scores[2], 5) == round(0.18666666666666668, 5)
    
    def test_score_values_2(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.18666666666666668, 5)
        assert round(scores[1], 5) == round(0.20512820512820512, 5)
        assert round(scores[2], 5) == round(0.09523809523809525, 5)
    
    def test_score_values_3(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9,
                       8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.03508771929824561, 5)
        assert round(scores[1], 5) == round(0.18666666666666668, 5)
        assert round(scores[2], 5) == round(0.2380952380952381, 5)
    
    def test_score_values_1_consider_neutral_False(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                consider_neutral=False)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.333333333, 5)
        assert round(scores[1], 5) == round(0.1, 5)
        assert round(scores[2], 5) == round(0.0, 5)
    
    def test_score_values_2_consider_neutral_False(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                consider_neutral=False)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.28, 5)
        assert round(scores[1], 5) == round(0.307692308, 5)
        assert round(scores[2], 5) == round(0.0, 5)
    
    def test_score_values_3_consider_neutral_False(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9,
                       8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                consider_neutral=False)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.052631579, 5)
        assert round(scores[1], 5) == round(0.28, 5)
        assert round(scores[2], 5) == round(0.0, 5)
    
    # Multi-currencies - Calibration on the 3 crypto pairs
    
    def test_multicurrencies_with_neutral(self):
        quotes_list_1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        quotes_list_2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        quotes_list_3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9,
                       8]
        foo_crypto_currency_pair_1 = generate_cryptocurrency_pair(quotes_list_1, name="BTC_EUR")
        foo_crypto_currency_pair_2 = generate_cryptocurrency_pair(quotes_list_2, name="ETH_EUR")
        foo_crypto_currency_pair_3 = generate_cryptocurrency_pair(quotes_list_3, name="LTC_EUR")
        
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair_1, foo_crypto_currency_pair_2, foo_crypto_currency_pair_3])
        
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'NEUTRAL'})
        assert best_hyperparameters == expected_hyperparameters

    def test_multicurrencies_without_neutral(self):
        quotes_list_1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        quotes_list_2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        quotes_list_3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9,
                       8]
        foo_crypto_currency_pair_1 = generate_cryptocurrency_pair(quotes_list_1, name="BTC_EUR")
        foo_crypto_currency_pair_2 = generate_cryptocurrency_pair(quotes_list_2, name="ETH_EUR")
        foo_crypto_currency_pair_3 = generate_cryptocurrency_pair(quotes_list_3, name="LTC_EUR")
        
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair_1, foo_crypto_currency_pair_2, foo_crypto_currency_pair_3])
        
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                consider_neutral=False)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        # Special case: same score, can be LONG or SHORT
        assert (best_hyperparameters == Hyperparameters({"signal": 'LONG'})) \
                or (best_hyperparameters == Hyperparameters({"signal": 'SHORT'}))

    def test_multicurrencies_with_neutral_value(self):
        quotes_list_1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        quotes_list_2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        quotes_list_3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9,
                       8]
        foo_crypto_currency_pair_1 = generate_cryptocurrency_pair(quotes_list_1, name="BTC_EUR")
        foo_crypto_currency_pair_2 = generate_cryptocurrency_pair(quotes_list_2, name="ETH_EUR")
        foo_crypto_currency_pair_3 = generate_cryptocurrency_pair(quotes_list_3, name="LTC_EUR")
        
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair_1, foo_crypto_currency_pair_2, foo_crypto_currency_pair_3])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.159624413, 5)
        assert round(scores[1], 5) == round(0.159624413, 5)
        assert round(scores[2], 5) == round(0.18018018, 5)

    def test_multicurrencies_without_neutral_value(self):
        quotes_list_1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        quotes_list_2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        quotes_list_3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9,
                       8]
        foo_crypto_currency_pair_1 = generate_cryptocurrency_pair(quotes_list_1, name="BTC_EUR")
        foo_crypto_currency_pair_2 = generate_cryptocurrency_pair(quotes_list_2, name="ETH_EUR")
        foo_crypto_currency_pair_3 = generate_cryptocurrency_pair(quotes_list_3, name="LTC_EUR")
        
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair_1, foo_crypto_currency_pair_2, foo_crypto_currency_pair_3])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                consider_neutral=False)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.23943662, 5)
        assert round(scores[1], 5) == round(0.23943662, 5)
        assert round(scores[2], 5) == round(0.0, 5)

    def test_scenario_1_step_2(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                calibration_step=2)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'LONG'})
        assert best_hyperparameters == expected_hyperparameters

    def test_scenario_2_step_3(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                calibration_step=3)

        best_hyperparameters = calibrator.calibrate(grid_config={})
        expected_hyperparameters = Hyperparameters({"signal": 'SHORT'})
        assert best_hyperparameters == expected_hyperparameters

    def test_score_values_1_step_2(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11,
                       10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                calibration_step=2)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.238095238, 5)
        assert round(scores[1], 5) == round(0.066666667, 5)
        assert round(scores[2], 5) == round(0.166666667, 5)

    def test_score_values_2_step_3(self):
        quotes_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8,
                       9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        foo_crypto_currency_pairs = CryptoCurrencyPairs([foo_crypto_currency_pair])
        calibrator = GridSearchCalibrator(strategy=self.foo_strategy, 
                                                cryptocurrency_pairs=foo_crypto_currency_pairs, 
                                                nb_data_training=10,
                                                calibration_step=3)

        grid = HyperparametersGridGenerator().generate_grid(self.foo_strategy.hyperparameters_boundary_constraints,
                                                            self.foo_strategy.hyperparameters_functional_constraints,
                                                            {})
        scores = calibrator.evaluate_model_combis(grid=grid)
        assert round(scores[0], 5) == round(0.166666667, 5)
        assert round(scores[1], 5) == round(0.266666667, 5)
        assert round(scores[2], 5) == round(0.0, 5)