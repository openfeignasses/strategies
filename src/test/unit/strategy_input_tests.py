import unittest
from strategies.inputs import StrategyInput
from cryptobot_commons import CryptoCurrencyPairBuilder


class StrategyInputTestCase(unittest.TestCase):

    def setUp(cls):
        cls.crypto_currency_pair = CryptoCurrencyPairBuilder("BTC_EUR", [{
            "open":0.0,
            "high":0.0,
            "low":0.0,
            "close": 1601.00000000,
            "vwap":0.0,
            "volume":0.0,
            "count":0.0,
            "time": 1234568
        }, {
            "open":0.0,
            "high":0.0,
            "low":0.0,
            "close": 1602.00000000,
            "vwap":0.0,
            "volume":0.0,
            "count":0.0,
            "time": 1234569
        }]).build()


    def test_instanciate_class(self):
        strategy_input = StrategyInput(self.crypto_currency_pair)
        self.assertIsInstance(strategy_input, StrategyInput)

    def test_wrong_constructor_parameter1(self):
        with self.assertRaises(Exception):
            StrategyInput({})

    def test_wrong_constructor_parameter2(self):
        with self.assertRaises(Exception):
            StrategyInput(None)
