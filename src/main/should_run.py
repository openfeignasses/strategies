from cryptobot_commons.retrievers import StrategiesResultsRetriever
from cryptobot_commons.retrievers import CryptoCurrencyPairsRetriever
from cryptobot_commons.retrievers import InvestmentUniverseStatusRetriever
import datetime
import sys

max_adddate_strategies = None
max_adddate_stock_quotations = None

strategies_results = StrategiesResultsRetriever("LIMITED_ORDERED", limit=1).execute().results
if strategies_results:
    last_strategies_result = strategies_results[0]
    max_adddate_strategies = last_strategies_result.add_date

retrieved_crypto_currency_pairs = CryptoCurrencyPairsRetriever("LAST_HOURS", name=None, limit=2).execute().pairs
if retrieved_crypto_currency_pairs:
    currency_pair = retrieved_crypto_currency_pairs[0]
    max_adddate_stock_quotations = currency_pair.close.data.index[0]

crashing_investment_universe = InvestmentUniverseStatusRetriever("CRASHING").execute()
print("crashing_investment_universe.status:"+str(crashing_investment_universe.status))

if crashing_investment_universe.status:
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: yes")
    sys.exit(0)
elif retrieved_crypto_currency_pairs and not strategies_results:
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: yes")
    sys.exit(0)
elif (retrieved_crypto_currency_pairs and strategies_results) and \
     (max_adddate_stock_quotations > max_adddate_strategies):
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: yes")
    sys.exit(0)
else:
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: no")
    sys.exit(1)