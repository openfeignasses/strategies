from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.strategy_result import StrategyResult
from cryptobot_commons.crypto_currency_pairs import CryptoCurrencyPairs


class Utils:
    @staticmethod
    def find_strategy_by_currency(strategies_results: StrategiesResults, currency_searched: str) -> StrategyResult:
        for result in strategies_results.results:
            if result.currency_pair.name == currency_searched:
                return result
        return None

    @staticmethod
    def find_currency_pair_by_name(currency_pairs: CryptoCurrencyPairs, currency_searched: str) -> StrategyResult:
        for result in currency_pairs.pairs:
            if result.name == currency_searched:
                return result
        return None
