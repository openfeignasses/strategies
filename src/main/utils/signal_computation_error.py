class SignalComputationError(Exception):

    def __init__(self, strategy_name):
        self.message = "Error: Impossible to compute a signal with the " + str(strategy_name) + " strategy"
        super().__init__(self.message)

    def __str__(self) -> str:
        return self.message