from typing import Tuple

from strategies.inputs import StrategyInput
from strategies.strategy import Strategy
from utils import SignalComputationError

from cryptobot_commons import Signal
from cryptobot_commons.hyperparameters import Hyperparameters, HyperparametersConstraints, IntBoundaryConstraint, \
    IsAboveFunctionalConstraint
import time

class RSI(Strategy):

    def __init__(self, hyperparameters: Hyperparameters):
        super().__init__("RSI")
        self.hyperparameters_dict = hyperparameters.entries
        self.hyperparameters_boundary_constraints = HyperparametersConstraints({
            "period": IntBoundaryConstraint(3, 15),
            "short_threshold": IntBoundaryConstraint(10, 40),
            "long_threshold": IntBoundaryConstraint(60, 90)
        })
        self.hyperparameters_functional_constraints = HyperparametersConstraints({
            "short_above_long": IsAboveFunctionalConstraint("long_threshold", "short_threshold")
        })

    def generate_signal(self, strategy_input: StrategyInput) -> Signal:
        period, short_threshold, long_threshold = self.__extract_parameters()
        close_price = strategy_input.crypto_currency_pair.close
        rsi = close_price.rsi(period)
        rsi_value = rsi.iloc[-1]
        self.__check_input_parameters(short_threshold, long_threshold, close_price, period)
        return self.__compute_rsi_signal(rsi_value, short_threshold, long_threshold)

    def __extract_parameters(self) -> Tuple[int, int, int]:
        period = self.hyperparameters_dict["period"]
        short_threshold = self.hyperparameters_dict["short_threshold"]
        long_threshold = self.hyperparameters_dict["long_threshold"]
        return period, short_threshold, long_threshold

    def __check_input_parameters(self, short_threshold, long_threshold, close_price, period):
        if short_threshold == long_threshold:
            raise ValueError("Error: RSI strategy has short_threshold == long_threshold")
        elif short_threshold > long_threshold:
            raise ValueError("Error: RSI strategy has short_threshold > long_threshold")
        elif len(close_price.data.index) <= period:
            raise ValueError("Error: RSI strategy has not enough data")

    def __compute_rsi_signal(self, rsi_value, short_threshold, long_threshold) -> Signal:
        if rsi_value > long_threshold:
            signal = Signal.SHORT
        elif rsi_value < short_threshold:
            signal = Signal.LONG
        elif (rsi_value >= short_threshold) and (rsi_value < 50):
            signal = Signal.SHORT
        elif (rsi_value <= long_threshold) and (rsi_value > 50):
            signal = Signal.LONG
        elif rsi_value == 50:
            signal = Signal.NEUTRAL
        else:
            raise SignalComputationError("RSI")
        return signal

    def train(self):
        """Not necessary for this strategy"""
        pass