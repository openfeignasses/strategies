from typing import Tuple

from strategies.inputs import StrategyInput
from strategies.strategy import Strategy
from utils import SignalComputationError

from cryptobot_commons import Signal
from cryptobot_commons.hyperparameters import Hyperparameters, HyperparametersConstraints, IntBoundaryConstraint, \
    IsAboveFunctionalConstraint


class MACD(Strategy):
     
    def __init__(self, hyperparameters: Hyperparameters):
        super().__init__("MACD")
        self.hyperparameters_dict = hyperparameters.entries
        self.hyperparameters_boundary_constraints = HyperparametersConstraints({
            "short_period": IntBoundaryConstraint(2, 40),
            "long_period": IntBoundaryConstraint(10, 60),
            "macd_ema_period": IntBoundaryConstraint(5, 20),
        })
        self.hyperparameters_functional_constraints = HyperparametersConstraints({
            "short_above_long": IsAboveFunctionalConstraint("long_period", "short_period")
        })
        
    def generate_signal(self, strategy_input: StrategyInput) -> Signal:
        short_period, long_period, macd_ema_period = self.__extract_parameters()
        close_price = strategy_input.crypto_currency_pair.close
        close_price.data = close_price.data.iloc[(-long_period-macd_ema_period-1):]
        macd = close_price.macd(short_period, long_period)
        macd_ema = close_price.macd_ema(short_period, long_period, macd_ema_period)
        self.__check_parameters_and_data(short_period, long_period, close_price, macd_ema_period)
        macd_1 = macd.iloc[-2] # MACD period t-1
        macd_2 = macd.iloc[-1] # MACD period t
        macd_ema_1 = macd_ema.iloc[-2] # MACD EMA period t-1
        macd_ema_2 = macd_ema.iloc[-1] # MACD EMA period t
        signal = self.__compute_macd_signal(macd_1, macd_2, macd_ema_1, macd_ema_2)
        return signal

    def __extract_parameters(self) -> Tuple[int, int, int]:
        short_period = self.hyperparameters_dict["short_period"]
        long_period = self.hyperparameters_dict["long_period"]
        macd_ema_period = self.hyperparameters_dict["macd_ema_period"]
        return short_period, long_period, macd_ema_period

    def __check_parameters_and_data(self, short_period, long_period, close_price, macd_ema_period):
        # If the short period is the same than the long one we cannot detect a change of the dynamic
        if short_period == long_period:
            raise ValueError("Error: MACD has short_period == long_period")
        # The short period has to be shorter than the long one (logic) 
        elif short_period > long_period:
            raise ValueError("Error: MACD has short_period > long_period")
        # When there is not enough data, we cannot compute the MACD
        elif len(close_price.data.index) <= long_period:
            raise ValueError("Error: MACD has not enough data")
        elif len(close_price.data.index) <= macd_ema_period:
            raise ValueError("Error: MACD has not enough data")

    def __compute_macd_signal(self, macd_1, macd_2, macd_ema_1, macd_ema_2) -> Signal:
        is_neutral = self.__is_neutral(macd_1, macd_2, macd_ema_1, macd_ema_2)
        if is_neutral:        
            signal = Signal.NEUTRAL
        elif macd_2 > macd_ema_2 and macd_1 > macd_ema_1:
            signal = Signal.LONG
        elif macd_2 < macd_ema_2 and macd_1 < macd_ema_1:
            signal = Signal.SHORT
        else:
            raise SignalComputationError("MACD")
        return signal

    def __is_neutral(self, macd_1, macd_2, macd_ema_1, macd_ema_2) -> bool:
        if macd_2 == macd_ema_2 and macd_1 == macd_ema_1 \
        or macd_2 > macd_ema_2 and macd_1 == macd_ema_1 \
        or macd_2 < macd_ema_2 and macd_1 == macd_ema_1 \
        or macd_2 == macd_ema_2 and macd_1 > macd_ema_1 \
        or macd_2 == macd_ema_2 and macd_1 < macd_ema_1 \
        or macd_2 < macd_ema_2 and macd_1 > macd_ema_1 \
        or macd_2 > macd_ema_2 and macd_1 < macd_ema_1:
            return True
        else:
            return False
            
    def train(self):
        """Not necessary for this strategy"""
        pass