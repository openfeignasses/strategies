from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.investment_universe import InvestmentUniverse


class StrategyInput:

    def __init__(self, crypto_currency_pair: CryptoCurrencyPair):
        self.crypto_currency_pair = crypto_currency_pair
        self.currency_pair = InvestmentUniverse[self.crypto_currency_pair.name]
        self._check_initialized_attributes()

    def _check_initialized_attributes(self):
        if not isinstance(self.crypto_currency_pair, CryptoCurrencyPair):
            raise (Exception("The crypto_currency_pair must be of type CryptoCurrencyPair."))