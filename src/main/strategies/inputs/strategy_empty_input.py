from cryptobot_commons.investment_universe import InvestmentUniverse


class StrategyEmptyInput:

    def __init__(self, currency_pair: InvestmentUniverse):
        self._check_input(currency_pair)
        self.currency_pair = currency_pair

    def _check_input(self, currency_pair: InvestmentUniverse):
        if not isinstance(currency_pair, InvestmentUniverse):
            raise Exception("currency_pair parameter cannot be None or empty.")
