from typing import Tuple

from strategies.inputs import StrategyInput
from strategies.strategy import Strategy
from utils import SignalComputationError

from cryptobot_commons import Signal
from cryptobot_commons.hyperparameters.hyperparameters_constraints import HyperparametersConstraints
from cryptobot_commons.hyperparameters.int_boundary_constraint import IntBoundaryConstraint
from cryptobot_commons.hyperparameters import Hyperparameters, IsAboveFunctionalConstraint


class EmaCrossovers(Strategy):
     
    def __init__(self, hyperparameters: Hyperparameters):
        super().__init__("EMA_CROSSOVERS")
        self.hyperparameters_dict = hyperparameters.entries
        self.hyperparameters_boundary_constraints = HyperparametersConstraints({
            "short_period": IntBoundaryConstraint(10, 60),
            "long_period": IntBoundaryConstraint(15, 99)
        })
        self.hyperparameters_functional_constraints = HyperparametersConstraints({
            "short_above_long": IsAboveFunctionalConstraint("long_period", "short_period")
        })

    def generate_signal(self, strategy_input: StrategyInput) -> Signal :
        short_period, long_period = self.__extract_parameters()
        close_price = strategy_input.crypto_currency_pair.close
        close_price.data = close_price.data.iloc[(-long_period-1):]
        ema_short = close_price.ema(short_period)
        ema_long = close_price.ema(long_period)
        self.__check_parameters_and_data(short_period, long_period, close_price)
        ema_short_1 = ema_short.iloc[-2] # EMA SHORT à la période t-1
        ema_short_2 = ema_short.iloc[-1] # EMA SHORT à la période t
        ema_long_1 = ema_long.iloc[-2] # EMA LONG à la période t-1
        ema_long_2 = ema_long.iloc[-1] # EMA LONG à la période t
        signal = self.__compute_ema_signal(ema_short_1, ema_short_2, ema_long_1, ema_long_2)
        return signal

    def __extract_parameters(self) -> Tuple[int, int]:
        short_period = self.hyperparameters_dict["short_period"]
        long_period = self.hyperparameters_dict["long_period"]
        return short_period, long_period

    def __check_parameters_and_data(self, short_period, long_period, close_price):
        # If the short period is the same than the long one we cannot detect a change of the dynamic
        if short_period == long_period:
            raise ValueError("Error: EMA_CROSSOVERS has short_period == long_period")
        # The short period has to be shorter than the long one (logic) 
        elif short_period > long_period:
            raise ValueError("Error: EMA_CROSSOVERS has short_period > long_period")
        # When there is not enough data, we cannot compute the EMA
        elif len(close_price.data.index) <= long_period:
            raise ValueError("Error: EMA_CROSSOVERS has not enough data")

    def __compute_ema_signal(self, ema_short_1, ema_short_2, ema_long_1, ema_long_2) -> Signal:
        is_neutral = self.__is_neutral(ema_short_1, ema_short_2, ema_long_1, ema_long_2)
        if is_neutral:        
            signal = Signal.NEUTRAL
        elif ema_short_2 > ema_long_2 and ema_short_1 > ema_long_1:
            signal = Signal.LONG
        elif ema_short_2 < ema_long_2 and ema_short_1 < ema_long_1:
            signal = Signal.SHORT
        else:
            raise SignalComputationError("EMA_CROSSOVERS")
        return signal

    def __is_neutral(self, ema_short_1, ema_short_2, ema_long_1, ema_long_2) -> bool:
        if (ema_short_2 == ema_long_2 and ema_short_1 == ema_long_1) \
        or (ema_short_2 > ema_long_2 and ema_short_1 == ema_long_1) \
        or (ema_short_2 < ema_long_2 and ema_short_1 == ema_long_1) \
        or (ema_short_2 == ema_long_2 and ema_short_1 > ema_long_1) \
        or (ema_short_2 == ema_long_2 and ema_short_1 < ema_long_1) \
        or (ema_short_2 < ema_long_2 and ema_short_1 > ema_long_1) \
        or (ema_short_2 > ema_long_2 and ema_short_1 < ema_long_1):
            return True
        else:
            return False

    def train(self):
        """Not necessary for this strategy"""
        pass