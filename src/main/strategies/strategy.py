from abc import ABC, abstractmethod
from strategies.inputs import StrategyInput
from strategies.inputs import StrategyEmptyInput

from cryptobot_commons import StrategyResult
from cryptobot_commons import Signal


class Strategy(ABC):

    model = None

    def __init__(self, name: str):
        self.name = name
        self._check_initialized_attributes()

    def _check_initialized_attributes(self):
        if not isinstance(self.name, str):
            raise TypeError("Error: The strategy name must be of type string.")

    def calculate_signal(self, strategy_input: StrategyInput) -> StrategyResult:
        self._check_strategy_input(strategy_input)
        self._check_length_above_10(strategy_input)
        signal = self.generate_signal(strategy_input)
        return self.generate_strategy_result(strategy_input, signal)

    def _check_strategy_input(self, strategy_input: StrategyInput):
        if not (isinstance(strategy_input, StrategyInput) or
                isinstance(strategy_input, StrategyEmptyInput)):
            raise TypeError("Error: The strategy_input must be of type StrategyInput or StrategyEmptyInput.")

    def _check_length_above_10(self, strategy_input: StrategyInput):
        limit = 10
        if len(strategy_input.crypto_currency_pair.open.data) < limit \
        or len(strategy_input.crypto_currency_pair.high.data) < limit \
        or len(strategy_input.crypto_currency_pair.low.data) < limit \
        or len(strategy_input.crypto_currency_pair.close.data) < limit \
        or len(strategy_input.crypto_currency_pair.vwap.data) < limit \
        or len(strategy_input.crypto_currency_pair.volume.data) < limit \
        or len(strategy_input.crypto_currency_pair.count.data) < limit:
            raise ValueError("Error: The " + str(self.name) + " strategy has not enough data, less than " + str(limit) + " observations "
                            "for " + str(strategy_input.crypto_currency_pair.name))
            
    def generate_strategy_result(self, strategy_input: StrategyInput, signal: Signal) -> StrategyResult:
        strategy_result_raw = {
            "strategy_name": self.name,
            "currency_pair": strategy_input.currency_pair,
            "signal": signal,
            "add_date": None,
            "execution_id": None
        }
        return StrategyResult(strategy_result_raw)

    @abstractmethod
    def generate_signal(self, strategy_input: StrategyInput):
        raise NotImplementedError

    @abstractmethod
    def train(self, strategy_input: StrategyInput):
        raise NotImplementedError