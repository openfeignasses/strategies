from strategies.inputs import StrategyEmptyInput
from strategies.strategy import Strategy
from cryptobot_commons import Signal


class SafetyFirst(Strategy):
 
    def __init__(self):
        super().__init__("SAFETY_FIRST")
    
    def generate_signal(self, strategy_input: StrategyEmptyInput) -> Signal:
        return Signal.NEUTRAL

    def _check_length_above_10(self, strategy_input: StrategyEmptyInput) -> bool:
        return True

    def train(self):
        """Not necessary for this strategy"""
        pass