from copy import deepcopy
from datetime import datetime
import time

from strategies.strategy import Strategy
from strategies.inputs.strategy_input import StrategyInput
from cryptobot_commons.classification_metrics import ClassificationMetrics
from cryptobot_commons.hyperparameters.hyperparameters_grid_generator import HyperparametersGridGenerator
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons import Signal
from cryptobot_commons.confusion_matrix_generator import ConfusionMatrixGenerator
from cryptobot_commons.hyperparameters import Hyperparameters
from cryptobot_commons import CryptoCurrencyPair, CryptoCurrencyPairs
from cryptobot_commons.book_of_confusion_matrixes import BookOfConfusionMatrixes

def find_best_hyperparameters(scores: list, grid: list) -> Hyperparameters:
    """
    Return the Hyperparameters object with the maximum score in the list of Hyperparameters (grid).
    """
    max_score = max(scores)
    return grid[scores.index(max_score)]


class GridSearchCalibrator:

    def __init__(self,
                strategy: Strategy,
                cryptocurrency_pairs: CryptoCurrencyPairs,
                nb_data_training: int,
                consider_neutral=True,
                calibration_step=1):
        self.strategy = deepcopy(strategy)
        self.cryptocurrency_pairs = cryptocurrency_pairs
        self.nb_data_training = nb_data_training
        self.consider_neutral = consider_neutral
        self.calibration_step = calibration_step

    def calibrate(self, grid_config: dict) -> Hyperparameters:
        start_time = time.time()
        grid = HyperparametersGridGenerator().generate_grid(self.strategy.hyperparameters_boundary_constraints,
                                                            self.strategy.hyperparameters_functional_constraints,
                                                            grid_config)
        scores = self.evaluate_model_combis(grid)
        print('Best Hyperparameters:', find_best_hyperparameters(scores, grid).entries)
        print('Execution time (in seconds):', time.time() - start_time)
        return find_best_hyperparameters(scores, grid)

    def evaluate_model_combis(self, grid: list) -> list:
        scores = []
        for hyperparameters in grid:
            self.strategy.hyperparameters_dict = hyperparameters.entries
            scores.append(self.__evaluate_one_model())
        return scores

    def __evaluate_one_model(self) -> float:
        list_strategies_results = []
        for pair in self.cryptocurrency_pairs.pairs:
            strategies_results_one_pair = self.evaluate_on_one_crypto_pair(pair)
            list_strategies_results.append(StrategiesResults(strategies_results_one_pair))

        book = BookOfConfusionMatrixes(list_strategies_results, self.cryptocurrency_pairs)
        book.write()
        confusion_matrix = book.get_sum()
        print("hyperparameters:", self.strategy.hyperparameters_dict, " - F1 Score:", ClassificationMetrics(confusion_matrix, self.consider_neutral).compute_f1_score())
        return ClassificationMetrics(confusion_matrix, self.consider_neutral).compute_f1_score()

    def evaluate_on_one_crypto_pair(self, pair: CryptoCurrencyPair):
        strategies_results_list = []
        # Below: "len(pair.close.data) - 4" because we need 5 data to evaluate a signal
        for nb_data_to_keep in range(self.nb_data_training + 1, len(pair.close.data) - 4, self.calibration_step):
            strategy_input = self.__generate_one_strategy_input(pair, nb_data_to_keep)
            result_dict = self.__generate_one_strategy_result(strategy_input)
            strategies_results_list.append(result_dict)
        return strategies_results_list

    def __generate_one_strategy_result(self, updated_strategy_input) -> dict:
        result = self.strategy.calculate_signal(updated_strategy_input)
        result.add_date = updated_strategy_input.crypto_currency_pair.close.data.index[-1]
        result_dict = result.to_dict()
        # add_date+1 to be just after the last date when doing the prediction
        result_dict['add_date'] = datetime.fromtimestamp(result_dict['add_date'] + 1)  
        result_dict['currency_pair'] = InvestmentUniverse[result_dict['currency_pair']]
        result_dict['signal'] = Signal[result_dict['signal']]
        return result_dict

    def __generate_one_strategy_input(self, pair: CryptoCurrencyPair, nb_data_to_keep: int) -> StrategyInput:
        """
        Return a new StrategyInput with a crypto_currency_pair's data updated inside.
        """
        strategy_input = StrategyInput(deepcopy(pair))
        strategy_input.crypto_currency_pair.close.data = \
            strategy_input.crypto_currency_pair.close.data.iloc[:nb_data_to_keep]
        return strategy_input
