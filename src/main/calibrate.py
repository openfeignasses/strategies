import json
import os
from calibration_scripts import SmaCrossoversCalibration
from calibration_scripts import EmaCrossoversCalibration
from calibration_scripts import MACDCalibration
from calibration_scripts import RSICalibration

strategy_to_calibrate = os.getenv("STRATEGY_TO_CALIBRATE")
calibrated_hyperparameters_object = None

if strategy_to_calibrate == "SMA_CROSSOVERS":
    calibrated_hyperparameters_object = SmaCrossoversCalibration().start()
elif strategy_to_calibrate == "EMA_CROSSOVERS":
    calibrated_hyperparameters_object = EmaCrossoversCalibration().start()
elif strategy_to_calibrate == "MACD":
    calibrated_hyperparameters_object = MACDCalibration().start()
elif strategy_to_calibrate == "RSI":
    calibrated_hyperparameters_object = RSICalibration().start()
else:
    print("The strategy " + strategy_to_calibrate + " has no associated calibration.")

if calibrated_hyperparameters_object is not None:
    file = open('calibrated_hyperparameters.json', 'w+')
    json.dump(calibrated_hyperparameters_object.entries, file)
    file.close()
else:
    print("Nothing to save.")

print("Calibration finished.")
