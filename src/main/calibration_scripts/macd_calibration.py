from strategies import MACD
from calibrators import GridSearchCalibrator
from strategies.inputs import StrategyInput

from cryptobot_commons.retrievers import CryptoCurrencyPairsRetriever
from cryptobot_commons.hyperparameters import HyperparametersLoader


class MACDCalibration:

    def start(self):
        currency_pairs = CryptoCurrencyPairsRetriever("ALL", None).execute()
        for pair in currency_pairs.pairs:
            pair.set_quantity_of_data(number_of_observations=500)
        
        macd = MACD(HyperparametersLoader("HYPERPARAMETERS_MACD").load())

        calibrator = GridSearchCalibrator(strategy=macd, 
                                                cryptocurrency_pairs=currency_pairs, 
                                                nb_data_training=100, 
                                                consider_neutral=False,
                                                calibration_step=4)
        grid_config = {'short_period': 10,
                       'long_period': 10,
                       'macd_ema_period': 5}

        return calibrator.calibrate(grid_config=grid_config)
