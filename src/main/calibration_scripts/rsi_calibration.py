from strategies import RSI
from calibrators import GridSearchCalibrator
from strategies.inputs import StrategyInput

from cryptobot_commons.retrievers import CryptoCurrencyPairsRetriever
from cryptobot_commons.hyperparameters import HyperparametersLoader


class RSICalibration:

    def start(self):
        currency_pairs = CryptoCurrencyPairsRetriever("ALL", None).execute()
        for pair in currency_pairs.pairs:
            pair.set_quantity_of_data(number_of_observations=500)

        rsi = RSI(HyperparametersLoader("HYPERPARAMETERS_RSI").load())

        calibrator = GridSearchCalibrator(strategy=rsi,
                                          cryptocurrency_pairs=currency_pairs,
                                          nb_data_training=100,
                                          consider_neutral=False,
                                          calibration_step=4)
        grid_config = {'period': 5,
                       'short_threshold': 4,
                       'long_threshold': 4}

        return calibrator.calibrate(grid_config=grid_config)
