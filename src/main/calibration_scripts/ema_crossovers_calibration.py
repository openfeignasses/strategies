from strategies import EmaCrossovers
from calibrators import GridSearchCalibrator
from strategies.inputs import StrategyInput

from cryptobot_commons.retrievers import CryptoCurrencyPairsRetriever
from cryptobot_commons.hyperparameters import HyperparametersLoader


class EmaCrossoversCalibration:

    def start(self):
        currency_pairs = CryptoCurrencyPairsRetriever("ALL", None).execute()
        for pair in currency_pairs.pairs:
            pair.set_quantity_of_data(number_of_observations=500)

        ema_crossovers = EmaCrossovers(HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load())

        calibrator = GridSearchCalibrator(strategy=ema_crossovers, 
                                                cryptocurrency_pairs=currency_pairs, 
                                                nb_data_training=100, 
                                                consider_neutral=False)
        grid_config = {'short_period': 30, 'long_period': 40}

        return calibrator.calibrate(grid_config=grid_config)
