from cryptobot_commons.retrievers import CryptoCurrencyPairsRetriever
from cryptobot_commons.retrievers import InvestmentUniverseStatusRetriever
from cryptobot_commons import StrategiesResults
from cryptobot_commons.savers import StrategiesResultsSaver
from strategies.macd import MACD
from cryptobot_commons.hyperparameters import HyperparametersLoader
from strategies.inputs import StrategyInput
from strategies.safety_first import SafetyFirst
from strategies.inputs import StrategyEmptyInput
from utils import Utils


currency_pairs = CryptoCurrencyPairsRetriever("ALL", None).execute()
strategies_results_list = []

# Instanciate Strategies
macd = MACD(HyperparametersLoader("HYPERPARAMETERS_MACD").load())
safety_first = SafetyFirst()

# TRADABLE Currency Pairs
tradable_currencies = InvestmentUniverseStatusRetriever("TRADABLE").execute()
for tradable_currency in tradable_currencies.status:
    currency_pair = Utils.find_currency_pair_by_name(currency_pairs, tradable_currency.name)
    strategy_input = StrategyInput(currency_pair)
    
    strategies_results_list.append(
        macd.calculate_signal(strategy_input)
    )

# NON_TRADABLE Currency Pairs
non_tradable_currencies = InvestmentUniverseStatusRetriever("NON_TRADABLE").execute()
for non_tradable_currency in non_tradable_currencies.status:
    strategy_input = StrategyEmptyInput(non_tradable_currency)

    strategies_results_list.append(
        safety_first.calculate_signal(strategy_input)
    )

# CRASHING Currency Pairs
crashing_currencies = InvestmentUniverseStatusRetriever("CRASHING").execute()
for crashing_currency in crashing_currencies.status:
    strategy_input = StrategyEmptyInput(crashing_currency)

    strategies_results_list.append(
        safety_first.calculate_signal(strategy_input)
    )

strategies_results = StrategiesResults(strategies_results_list)
strategies_results_saver = StrategiesResultsSaver(strategies_results)
strategies_results_saver.save()

print("Execution finished.")
